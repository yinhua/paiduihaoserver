package ishareserver

import grails.converters.JSON
import java.text.SimpleDateFormat

class QueueController {
	def queueDynaService
	
    def index() { 
		switch(request.method){
			case "POST":
			
			break;
			
			case "GET":
				def insATN  = params.insATN
				QueueStatus qs = queueDynaService.getQueueStatus(insATN)
				render qs as JSON
			
			break;
			
			case "PUT":
				def jsonObj = request.JSON
				def dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
				
				Calendar cins = Calendar.getInstance();
				TimeZone tz  = TimeZone.getTimeZone("PRC");
				cins.setTimeZone(tz);
				
				def date = cins.getTime()
				def prcTimeNow = dateFormatter.format(date);
				
				jsonObj.commitTime = prcTimeNow
				
				QueueRecord qrec= new QueueRecord(jsonObj)
				QueueStatus qs = queueDynaService.updateQueue(qrec)
				render qs as JSON
				
			break;
			
			case "DELETE":
			
			break;
		}
		
	}
}
