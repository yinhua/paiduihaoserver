package ishareserver



import static org.springframework.http.HttpStatus.*
import com.amazonaws.services.ec2.model.Instance
import com.amazonaws.services.ec2.model.Reservation
import grails.converters.JSON
import grails.transaction.Transactional
import redis.clients.jedis.Jedis


class InstitutionTypeController {
	def amazonWebService
    //static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
	def jedisConPoolService
	def queueDynaService
	
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
	
		
		def institutionTypeInstance = new InstitutionType();
		//def institutionTypeInstance = Hello.newInstance([typeId:1, typeName:'hospital']);
		//institutionTypeInstance.setVariable("typeId", 1);
		def inst = amazonWebService.dynamoDBMapper.load(InstitutionType.class,3)
		//Jedis jed  = jedisConPoolService.getPool().getResource()
		//jed.set("hello","hello ,jedis!")
		//println"${jed.get("hello")}"
		
		//queueDynaService.redisExpireTest()
		
		render inst as JSON;
		
		/*
		institutionTypeInstance.typeId = 5
		institutionTypeInstance.typeName = "test"
		println institutionTypeInstance.typeName
		amazonWebService.dynamoDBMapper.save(institutionTypeInstance)
		*/

		/*
		List reservations = amazonWebService.ec2.describeInstances().reservations
		reservations.each { Reservation reservation ->
			println "reservationId: ${reservation.reservationId}"
			reservation.instances.each() { Instance instance ->
				println "instanceId: ${instance.instanceId}, imageId: ${instance.imageId}, instanceType: ${instance.instanceType}"
			}
		}
		*/
		
       // respond InstitutionType.list(params), model:[institutionTypeInstanceCount: InstitutionType.count()]
    }

}
