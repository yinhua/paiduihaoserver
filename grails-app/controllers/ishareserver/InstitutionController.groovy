package ishareserver

import grails.converters.JSON

import java.text.SimpleDateFormat

class InstitutionController {
	def amazonWebService
	def institutionDynaService
	
	
    def index() { 
		switch(request.method){
			case "POST":
				def jsonObj = request.JSON
				
				def dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
				//dateFormatter.setTimeZone(TimeZone.getTimeZone("PRC"));
				
				Calendar cins = Calendar.getInstance();
				TimeZone tz  = TimeZone.getTimeZone("PRC");
				cins.setTimeZone(tz);
				
				def date = cins.getTime()
				def prcTimeNow = dateFormatter.format(date);
				
				jsonObj.createTime = prcTimeNow
				
				def institution = new Institution(jsonObj)
				
				//amazonWebService.dynamoDBMapper.save(institution)
				institutionDynaService.saveInstitution(institution)
				
				render institution as JSON
				
				
			break
			
			
			case "GET":
				
				def name  = params.name
				def areaTypeCode =  params.areaTypeCode
				def filter = params.filter
				def res
				if(filter == null){
					res = institutionDynaService.loadInstitutionByATN(areaTypeCode, name)
					
				}else if(filter.equals("fourGet")){
					// just area code without type code
					res = institutionDynaService.getFourRandomInstitution(areaTypeCode)
				}else{
					res = institutionDynaService.queryInstitutionsByPartOfName(areaTypeCode, name)
				}
				
				
				render res as JSON
			break
			
			
			case "PUT":
				def jsonObj = request.JSON
				
				
				def institution = new Institution(jsonObj)
				
				institutionDynaService.saveInstitution(institution)
				
				render institution as JSON
			
			break
			
			
			case "DELETE":
				def name  = params.name
				def areaTypeCode =  params.areaTypeCode
				institutionDynaService.deleteInstitution(areaTypeCode, name)
				render "successfull"
			break
		}
	}
}
