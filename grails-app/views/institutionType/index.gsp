
<%@ page import="ishareserver.InstitutionType" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'institutionType.label', default: 'InstitutionType')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-institutionType" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-institutionType" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="typeId" title="${message(code: 'institutionType.typeId.label', default: 'Type Id')}" />
					
						<g:sortableColumn property="typeName" title="${message(code: 'institutionType.typeName.label', default: 'Type Name')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${institutionTypeInstanceList}" status="i" var="institutionTypeInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${institutionTypeInstance.id}">${fieldValue(bean: institutionTypeInstance, field: "typeId")}</g:link></td>
					
						<td>${fieldValue(bean: institutionTypeInstance, field: "typeName")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${institutionTypeInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
