<%@ page import="ishareserver.InstitutionType" %>



<div class="fieldcontain ${hasErrors(bean: institutionTypeInstance, field: 'typeId', 'error')} ">
	<label for="typeId">
		<g:message code="institutionType.typeId.label" default="Type Id" />
		
	</label>
	<g:field type="number" name="typeId" value="${institutionTypeInstance.typeId}" />

</div>

<div class="fieldcontain ${hasErrors(bean: institutionTypeInstance, field: 'typeName', 'error')} ">
	<label for="typeName">
		<g:message code="institutionType.typeName.label" default="Type Name" />
		
	</label>
	<g:textField name="typeName" value="${institutionTypeInstance?.typeName}" />

</div>

