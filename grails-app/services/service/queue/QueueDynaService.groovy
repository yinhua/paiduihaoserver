package service.queue

import grails.transaction.Transactional
import ishareserver.QueueRecord
import ishareserver.QueueStatus

import javax.annotation.PostConstruct

import redis.clients.jedis.Jedis

import java.util.concurrent.Callable
import java.util.concurrent.Executors;

@Transactional
class QueueDynaService {
	def amazonWebService
	def jedisConPoolService
	def fixedThreadPool
	def grailsApplication
	
	@PostConstruct
	def init(){
		def fixedNu = Integer.valueOf( grailsApplication.config.ishare.queueRecord.persistence.thread.number )
		fixedThreadPool = Executors.newFixedThreadPool(fixedNu)
	}
	
    def shutdownTheadPool() {
		fixedThreadPool.shutdown()
    }
	
	def redisExpireTest(){
		println "测试开始"
		Jedis jds = jedisConPoolService.getPoolCon().getResource()
		println "return value from not exist : ${jds.get("notExist")}"
		if(jds.get("notExist") == null){
			println "i am null, == null can be used."
		}
		
		jds.setex("1s",1,"expire after 1 seconds")
		Thread.sleep(2000)
		def tstr = jds.get("1s")
		println "${tstr}"
		if(tstr == null){
			println "i am null, == null can be used."
		}else{
			println "== null can not be used, value: ${tstr}"
		}
		println "return value from expired key : ${jds.get("1s")}"
		
		jds.setex("120s",120,"expire after 120 seconds")
		println"${jds.get("120s")} , ttl: ${jds.ttl("120s")}"
		
		jds.set("120s","expire after 120 seconds,but content changed")
		
		println"${jds.get("120s")} , ttl: ${jds.ttl("120s")}"
		
		
		jds.set("中文测试","中文测试值啦")
		println"Chinese test: ${jds.get("中文测试")}"
		
		HashMap myMa = new HashMap();
		myMa.put("insATN", "021-01:某某医院")
		myMa.put("number", "12")
		myMa.put("commitTime", "20140211105230")
		myMa.put("commitUser", "edjon")
		
		jds.hmset("021-01:某某医院",myMa)
		jds.expire("021-01:某某医院",3600)
		println "hash set ,021-01:某某医院, number: ${jds.hget("021-01:某某医院" ,"number")}, ttl: ${jds.ttl("021-01:某某医院")}"
		println "sleep 5 seconds"
		Thread.sleep(7000)
		//jds.hincrBy("021-01:某某医院","number",20)
		//jds.hset("021-01:某某医院" ,"number","22")
		HashMap qr = new HashMap()
		qr.put("number", "11")
		qr.put("commitUser", "edwin")
		jds.hmset("021-01:某某医院" ,qr)
		println "hash set ,021-01:某某医院, number changed. number: ${jds.hget("021-01:某某医院" ,"number")}, ttl: ${jds.ttl("021-01:某某医院")}"
		
		def map = jds.hgetAll("021-01:某某医院")
		map.each{k,v->
			if(k.equals("number"))println"nubmer :${v}"
			else if(k.equals("commitUser"))println"commitUser :${v}"
		  }
		println"exists(String key) value: ${jds.exists("021-01:某某医院")}"
		
	}
	
	Set<String> getFourKey(){
		Set<String> keySet = new HashSet();
		Jedis jds = jedisConPoolService.getPoolCon().getResource()
		for(int i = 0; i<4; i++){
			String key = jds.randomKey();
			if(key != null && key.length() > 0)keySet.add(key)
		}
		
		jedisConPoolService.getPoolCon().returnResource(jds);
		return keySet
	}
	
	
	QueueStatus updateQueue(QueueRecord queueRecord){
		//String insATN, Integer number, String commitTime, String commitUser
		QueueStatus qsta = new QueueStatus()
		
		//1,update cache
		Jedis jds = jedisConPoolService.getPoolCon().getResource()
		
		
		boolean keyExists =  jds.exists(queueRecord.insATN)
		
		HashMap qr = new HashMap()
		qr.put("number", queueRecord.number)
		qr.put("commitUser", queueRecord.commitUser)
		jds.hmset(queueRecord.insATN , qr)
		if(!keyExists)jds.expire(queueRecord.insATN, getSecondsLeftToday())
		
		qsta.insATN = queueRecord.insATN
		qsta.number = queueRecord.number
		qsta.lastUpdateUser = queueRecord.commitUser
		
		jedisConPoolService.getPoolCon().returnResource(jds);
		
		//2,update to QueueStatus and save to QueueRecord
		
		fixedThreadPool.submit({->
			saveToDynamo queueRecord } as Callable);
		
		
		return qsta
	}
	/**
	 * In oder to get latest queue number , get Queue Status from cache first,
	 * if doesn't exist, try to get it from dynamodb . 
	 * @param insATNs
	 * @return
	 */
	QueueStatus getQueueStatus(String insATN){
		Jedis jds = jedisConPoolService.getPoolCon().getResource()
		
		QueueStatus qsta  
		
		def statusJson = jds.hgetAll(insATN)	
		
		jedisConPoolService.getPoolCon().returnResource(jds);
		
		
		//if(statusJson.equals("nil")){// cache miss. try to find latest value from dynamodb.
		if(statusJson.size() == 0){
			qsta = amazonWebService.dynamoDBMapper.load(QueueStatus.class, insATN)
			if(qsta ==  null){
				qsta  = new QueueStatus()
				qsta.insATN = insATN
				qsta.number = 3
				
			}
		}else{
			qsta  = new QueueStatus()
			
			//["number":"12", "lastUpdateUser":"web"]
			statusJson.each{k,v->
				if(k.equals("number"))qsta.number = v
				else if(k.equals("commitUser"))qsta.lastUpdateUser = v
			  }
			qsta.insATN = insATN
		} 
		
		return qsta
	}
	
	
	def getSecondsLeftToday(){
		
		Calendar cins = Calendar.getInstance();
		TimeZone tz  = TimeZone.getTimeZone("PRC");
		cins.setTimeZone(tz);
		
		long now  = cins.getTimeInMillis();
		cins.add(Calendar.DATE, 1);
		
		
		cins.set(Calendar.HOUR_OF_DAY, 0);
		cins.set(Calendar.MINUTE, 0);
		cins.set(Calendar.SECOND, 0);
		cins.set(Calendar.MILLISECOND, 0);
		
	
		long fu = cins.getTimeInMillis();
		long sec  = (fu- now)/1000
		return Integer.valueOf( "" + sec)
		
	}
	
	

		
	
	def saveToDynamo(QueueRecord queueRecord){
		
		QueueStatus qsta  = new QueueStatus()
		qsta.insATN = queueRecord.insATN
		qsta.number = queueRecord.number
		qsta.lastUpdateUser = queueRecord.commitUser
		amazonWebService.dynamoDBMapper.save(qsta)
		amazonWebService.dynamoDBMapper.save(queueRecord)
	}
	
}
