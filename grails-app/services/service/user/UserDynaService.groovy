package service.user

import grails.transaction.Transactional
import ishareserver.User

@Transactional
class UserDynaService {
	def amazonWebService
    def serviceMethod() {

    }
	
	User loadUser(String msisdn){
		User user  = amazonWebService.dynamoDBMapper.load(User.class, msisdn)
		return user
	}
	
	User loginUser(String msisdn, String nickName){
		User user  = amazonWebService.dynamoDBMapper.load(User.class, msisdn)
		if(user ==null){
			User saveUser  = new User();
			saveUser.msisdn = msisdn
			saveUser.nickName = nickName
			amazonWebService.dynamoDBMapper.save(saveUser)
			return saveUser
		}else{
			
			String nkName =  user.nickName;
			if(nkName.equals(nickName)){
				return user
			}else{
				return null
			}
		}
	}
}
