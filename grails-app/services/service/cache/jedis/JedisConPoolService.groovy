package service.cache.jedis

import grails.transaction.Transactional
import javax.annotation.PostConstruct
import redis.clients.jedis.JedisPool
import redis.clients.jedis.JedisPoolConfig

@Transactional
class JedisConPoolService {
	def pool
	def grailsApplication
	
	
	@PostConstruct
	def init(){
	
		JedisPoolConfig jeConf = new JedisPoolConfig()
		jeConf.setMaxIdle(Integer.valueOf(grailsApplication.config.redis.client.jedis.pool.maxIdle))
		jeConf.setMaxTotal(Integer.valueOf(grailsApplication.config.redis.client.jedis.pool.maxActive))
		jeConf.setMaxWaitMillis(Integer.valueOf(grailsApplication.config.redis.client.jedis.pool.maxWait))
		jeConf.setTestOnBorrow(Boolean.valueOf(grailsApplication.config.redis.client.jedis.pool.testOnBorrow))
		jeConf.setTestOnReturn(Boolean.valueOf(grailsApplication.config.redis.client.jedis.pool.testOnReturn))
		
	
		pool =  new JedisPool(jeConf, "cachet2micro.nwxgwu.0001.apne1.cache.amazonaws.com",
				Integer.valueOf(grailsApplication.config.redis.client.jedis.pool.port));
	}
    def serviceMethod() {

    }
	def pt(){
		def config = grailsApplication.config.redis.client.jedis.pool.maxActive

		println"${config}"
	}
	
	JedisPool getPoolCon(){
		return pool
	}
	
	def destoryPool(){
		pool.destroy();
	}
}
