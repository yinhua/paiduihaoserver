package service.institution

import java.sql.ResultSet;
import java.util.Map;
import java.util.HashMap;

import com.amazonaws.auth.DefaultAWSCredentialsProviderChain
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider
import com.amazonaws.auth.profile.ProfileCredentialsProvider
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression
import com.amazonaws.services.dynamodbv2.model.AttributeValue
import com.amazonaws.services.dynamodbv2.model.Condition
import com.amazonaws.services.dynamodbv2.model.ScanRequest
import com.amazonaws.services.dynamodbv2.model.ScanResult

import grails.transaction.Transactional
import ishareserver.Institution

import com.amazonaws.services.dynamodbv2.model.ComparisonOperator

@Transactional
class InstitutionDynaService {
	def amazonWebService
	def queueDynaService
    def serviceMethod() {

    }
	
	
	List<Institution> getFourRandomInstitution(String areaCode ){
		
		List<Institution> fourList = new ArrayList<Institution>()
				
		for(int i= 1; i<=4; i++){
			
			String atc = areaCode + "-"+i;
			Institution insKey = new Institution()
			insKey.setInsAT(atc)
			
			DynamoDBQueryExpression<Institution> dbqe = new DynamoDBQueryExpression<Institution>()
			dbqe.withHashKeyValues(insKey).setLimit(4)
			
//			Map<String, Condition> queryFilter = new HashMap<String, Condition>()
//			
//			queryFilter.put("desc",new Condition().withComparisonOperator(ComparisonOperator.CONTAINS)
//				.withAttributeValueList( new AttributeValue().withS("上海")))
//			queryExpression.withQueryFilter(queryFilter)
			
			
			List<Institution> result = amazonWebService.dynamoDBMapper.query(Institution.class, dbqe);
			if(result != null && result.size() > 0){
				fourList.add(result.get(0));
			}
		}

		if(fourList.size()== 0){
			 
			Set atnSet = queueDynaService.getFourKey()
			if(atnSet !=null && atnSet.size() > 0){
				def e
				atnSet.each { e = it 
					if(!e.contains("ElastiCache")) {
						String[] atnItems = e.split("-")
						String insAt = atnItems[0] + "-" + atnItems[1]
						String name =  atnItems[2]
						
						Institution insti  = loadInstitutionByATN( insAt, name)
						fourList.add(insti)
					}
					
					
					//println"${e}"
				}
				 
				
			}
			
		
		}
		
		return fourList;
	}
	
	List<Institution> queryInstitutionsByPartOfName(String areaTypeCode,String nameKey){
		Institution insKey = new Institution()
		insKey.setInsAT(areaTypeCode)
		/*
		Condition rangeKeyCodeCondition = new Condition().withComparisonOperator(ComparisonOperator.EQ)
		.withAttributeValueList( new AttributeValue().withS(name))*/
		
		Map<String, Condition> queryFilter = new HashMap<String, Condition>()
		queryFilter.put("desc",new Condition().withComparisonOperator(ComparisonOperator.CONTAINS)
			.withAttributeValueList( new AttributeValue().withS(nameKey)))
		
		
		DynamoDBQueryExpression<Institution> queryExpression = new DynamoDBQueryExpression<Institution>()
		.withHashKeyValues(insKey)
		.withQueryFilter(queryFilter)
		//.withRangeKeyCondition("desc", new Condition().withComparisonOperator(ComparisonOperator.CONTAINS)
			//.withAttributeValueList( new AttributeValue().withS(nameKey)))
		
		
		List<Institution> result = amazonWebService.dynamoDBMapper.query(Institution.class, queryExpression);
		
		return result
	}
	
	Institution loadInstitutionByATN(String areaTypeCode,String name){
		Institution ins = amazonWebService.dynamoDBMapper.load(Institution.class,areaTypeCode,name)
		return ins
	}
	
	Institution saveInstitution(Institution ins){
		def insN = ins.name
		if(insN == null || insN.length() == 0) return null
		amazonWebService.dynamoDBMapper.save(ins)
		return ins
	}
	
	Institution deleteInstitution(String areaTypeCode,String name){
		Institution ins = amazonWebService.dynamoDBMapper.load(Institution.class,areaTypeCode,name)
		amazonWebService.dynamoDBMapper.delete(ins);
		return amazonWebService.dynamoDBMapper.load(Institution.class,areaTypeCode,name)
	}
}
