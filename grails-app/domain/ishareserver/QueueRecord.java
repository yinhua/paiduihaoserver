package ishareserver;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

@DynamoDBTable(tableName="QueueRecord")
public class QueueRecord {
	String insATN;
	String commitTime;
	String number;
	String commitUser;
	
	@DynamoDBHashKey(attributeName="insATN")
	public String getInsATN() {
		return insATN;
	}
	public void setInsATN(String insATN) {
		this.insATN = insATN;
	}
	
	@DynamoDBRangeKey(attributeName="commitTime")
	public String getCommitTime() {
		return commitTime;
	}
	public void setCommitTime(String commitTime) {
		this.commitTime = commitTime;
	}
	
	
	@DynamoDBAttribute
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	
	@DynamoDBAttribute
	public String getCommitUser() {
		return commitUser;
	}
	public void setCommitUser(String commitUser) {
		this.commitUser = commitUser;
	}
	
	
}
