package ishareserver;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBIndexRangeKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;


@DynamoDBTable(tableName="Institution")
public class Institution {
	
	String insAT;
	String address;
	String author;
	String createTime;
	String desc;
	String name;
	String subs;
	
	
	
	@DynamoDBHashKey(attributeName="insAT")
	public String getInsAT() {
		return insAT;
	}
	public void setInsAT(String insAT) {
		this.insAT = insAT;
	}
	
	
	@DynamoDBRangeKey(attributeName="name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
	@DynamoDBAttribute
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	
	@DynamoDBAttribute
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	
	@DynamoDBAttribute
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	
	
	@DynamoDBIndexRangeKey(attributeName="desc", localSecondaryIndexName="desc-index")
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	@DynamoDBAttribute
	public String getSubs() {
		return subs;
	}
	public void setSubs(String subs) {
		this.subs = subs;
	}
	
	
	
}
