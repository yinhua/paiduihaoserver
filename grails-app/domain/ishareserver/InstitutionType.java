package ishareserver;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;


@DynamoDBTable(tableName="InstitutionType")
 public class InstitutionType {
		Integer typeId;
		String  typeName;
//		InstitutionType(Integer typeId, String typeName){
//			this.typeId = typeId;
//			this.typeName = typeName;
//		}
		
//    static constraints = {
//		typeId(nullable:false,blank:false)
//		typeName(nullable:false, blank:false)
//    }
	//amazonWebService.s3.createBucket('some-grails-bucket')
	@DynamoDBHashKey(attributeName="typeId")
	public Integer getTypeId() {
		return typeId;
	}
	public void setTypeId(Integer typeId) {
		this.typeId = typeId;
	}
	
	@DynamoDBAttribute
	public String getTypeName() {
		return typeName;
	}
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	
	
}
