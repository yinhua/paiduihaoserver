package ishareserver;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

@DynamoDBTable(tableName="QueueStatus")
public class QueueStatus {
	String insATN;
	String number;
	String lastUpdateUser;
	
	@DynamoDBHashKey(attributeName="insATN")
	public String getInsATN() {
		return insATN;
	}
	public void setInsATN(String insATN) {
		this.insATN = insATN;
	}
	
	@DynamoDBAttribute
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	
	@DynamoDBAttribute
	public String getLastUpdateUser() {
		return lastUpdateUser;
	}
	public void setLastUpdateUser(String lastUpdateUser) {
		this.lastUpdateUser = lastUpdateUser;
	}
	
	
	
}
